### JVM demo for build of Gradle/Springboot, build of Dockerfile with Registry and deploy using Helm to Openshift 4.11

Description: The Springboot demo is how to put the jvm options in Openshift configmap and secret.

Note: Openshift 4.11, Gitlab.com, helm Gitlab Runner Community was used on this.

```
Follow the ocp/notes.txt

oc expose service/jvm
output:
$ curl "openshift-url-route"
Configmap: tmpuser ----------- Secret: mysecret
```

### test